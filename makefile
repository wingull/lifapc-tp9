#makefile template for a C++ project
#project name

PROJ_NAME = tp9.exe

#directory for compiled binaries

BIN_DIR = bin

#directory for object files

OBJ_DIR = obj

#directory for source files

SRC_DIR = src

#C++ compiler and flags

CC = g++
CFLAGS = -Wall -std=c++11 -Werror -fsanitize=address -g

#list of source files

SOURCES = $(wildcard $(SRC_DIR)/*.cpp)

#list of object files

OBJECTS = $(SOURCES:$(SRC_DIR)/%.cpp=$(OBJ_DIR)/%.o)

#default target

all: $(BIN_DIR)/$(PROJ_NAME)

#build target

$(BIN_DIR)/$(PROJ_NAME): $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) -o $@

#compile source files

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CC) $(CFLAGS) -c $< -o $@

#clean target

clean:
	rm -f $(OBJ_DIR)/*.o $(BIN_DIR)/*

#run target

run: all
	./$(BIN_DIR)/$(PROJ_NAME)

#debug target

debug: CFLAGS += -g
debug: all
	gdb $(BIN_DIR)/$(PROJ_NAME)