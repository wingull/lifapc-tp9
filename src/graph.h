#ifndef _GRAPH
#define _GRAPH

#include "queue.h"
#include <string>


class Graph {
    private:
        int size;
        int xSize;
        int ySize;
        int* graph;
    public:
        Graph(int xSize, int ySize);
        Graph(std::string filename);
        ~Graph();
        int getSize();
        int getXSize();
        int getYSize();
        int get(int x, int y);
        void set(int x, int y, int value);
        void print();
        double getDistance(int x1, int y1, int x2, int y2);
        void dijkstra(int x1, int y1, element* tabElement);
        void printElement(element* tabElement);


};

#endif