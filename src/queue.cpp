#include "queue.h"
#include <iostream>

queue::queue()
{
    tabElement = NULL;
    size = 0;
    maxSize = 0;
}

queue::queue(int maxSize)
{
    tabElement = new element[maxSize];
    size = 0;
    this->maxSize = maxSize;
}

queue::~queue()
{
    delete[] tabElement;
}

void queue::push(int x, int y, double distance, element* next)
{
    if (size < maxSize)
    {
        tabElement[size].x = x;
        tabElement[size].y = y;
        tabElement[size].distance = distance;
        tabElement[size].next = next;
        size++;
    }
    else
    {
        std::cout<<"Queue is full"<<std::endl;
    }
}

element queue::pop()
{
    if (size > 0)
    {
        element temp = tabElement[0];
        for (int i = 0; i < size-1; i++)
        {
            tabElement[i] = tabElement[i+1];
        }
        size--;
        return temp;
    }
    else
    {
        std::cout<<"Queue is empty"<<std::endl;
        return tabElement[0];
    }
}

bool queue::isEmpty()
{
    if (size == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void queue::print()
{
    for (int i = 0; i < size; i++)
    {
        std::cout<<"x: "<<tabElement[i].x<<" y: "<<tabElement[i].y<<" distance: "<<tabElement[i].distance<<std::endl;
    }
}

