#include "graph.h"
#include "queue.h"
#include <iostream>
#include <fstream>
#include <string>
#include <math.h>

Graph::Graph(int xSize, int ySize)
{
    this->xSize = xSize;
    this->ySize = ySize;
    size = xSize * ySize;
    graph = new int[size];
    for (int i = 0; i < size; i++)
    {
        graph[i] = 0;
    }
}

Graph::Graph(std::string filename)
{
    std::ifstream file(filename);
    if (file.is_open())
    {
        //cin size
        file>>xSize;
        file>>ySize;
        size = xSize * ySize;
        graph = new int[size];
        //cin graph
        for (int i = 0; i < size; i++)
        {
            file>>graph[i];
        }
    }
    else
    {
        std::cout<<"File not found"<<std::endl;
    }
}

Graph::~Graph()
{
    delete[] graph;
}

int Graph::getSize()
{
    return size;
}

int Graph::getXSize()
{
    return xSize;
}

int Graph::getYSize()
{
    return ySize;
}

int Graph::get(int x, int y)
{
    return graph[x + y * xSize];
}

void Graph::set(int x, int y, int value)
{
    graph[x + y * xSize] = value;
}

void Graph::print()
{
    for (int i = 0; i < ySize; i++)
    {
        for (int j = 0; j < xSize; j++)
        {
            std::cout<<graph[j + i * xSize]<<" ";
        }
        std::cout<<std::endl;
    }
}

double Graph::getDistance(int x1, int y1, int x2, int y2)
{
    //distance in 3D with z in the graph
    return sqrt(pow(x1 - x2, 2) + pow(y1 - y2, 2) 
        + pow(get(x1, y1) - get(x2, y2), 2));
}

void Graph::dijkstra(int x1, int y1, element* tabElement)
{
    //init
    for (int i = 0; i < size; i++)
    {
        tabElement[i].distance = -1;
        tabElement[i].next = NULL;
    }
    queue queue(size*4);
    queue.push(x1, y1, 0, NULL);
    //dijkstra
    while (!queue.isEmpty())
    {
        element temp = queue.pop();
        if (tabElement[temp.x + temp.y * xSize].distance == -1)
        {
            tabElement[temp.x + temp.y * xSize].distance = temp.distance;
            tabElement[temp.x + temp.y * xSize].next = temp.next;
            //add neighbours
            //left
            if (temp.x > 0)
            {
                queue.push(temp.x - 1, temp.y, temp.distance 
                    + getDistance(temp.x, temp.y, temp.x - 1, temp.y), 
                    &tabElement[temp.x + temp.y * xSize]);
            }
            //right
            if (temp.x < xSize - 1)
            {
                queue.push(temp.x + 1, temp.y, temp.distance 
                    + getDistance(temp.x, temp.y, temp.x + 1, temp.y), 
                    &tabElement[temp.x + temp.y * xSize]);
            }
            //up
            if (temp.y > 0)
            {
                queue.push(temp.x, temp.y - 1, temp.distance 
                    + getDistance(temp.x, temp.y, temp.x, temp.y - 1), 
                    &tabElement[temp.x + temp.y * xSize]);
            }
            //down
            if (temp.y < ySize - 1)
            {
                queue.push(temp.x, temp.y + 1, temp.distance 
                    + getDistance(temp.x, temp.y, temp.x, temp.y + 1), 
                    &tabElement[temp.x + temp.y * xSize]);
            }
        }
    }
}

void Graph::printElement(element* tabElement)
{
    for (int i = 0; i < ySize; i++)
    {
        for (int j = 0; j < xSize; j++)
        {
            std::cout<<tabElement[j + i * xSize].distance<<" ";
        }
        std::cout<<std::endl;
    }
}

