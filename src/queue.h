#ifndef _QUEUE
#define _QUEUE

#include <iostream>


//class queue used for priority queue in dijkstra algorithm
struct element {
    int x;
    int y;
    double distance;
    element* next;
};

class queue {
    private:
        element* tabElement;
        int size;
        int maxSize;
    public:
        queue();
        queue(int maxSize);
        ~queue();
        void push(int x, int y, double distance, element* next);
        element pop();
        bool isEmpty();
        void print();
};



#endif