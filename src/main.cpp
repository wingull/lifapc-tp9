#include <iostream>
#include <fstream>
#include <string>
#include <cstring>

#include "graph.h"

const std::string filename = "test.txt";
//const std::string filename = "BIGcarte.txt";

int main()
{

    std::cout<<"__________________"<<std::endl<<std::endl;

    // Graph graph(filename);
    Graph graph(filename);
    graph.print();
    std::cout<<"__________________"<<std::endl<<std::endl;
    //test dijkstra
    element* tabElement = new element[graph.getSize()];
    graph.dijkstra(2, 1, tabElement);
    graph.printElement(tabElement);
    std::cout<<"__________________"<<std::endl<<std::endl;
    graph.dijkstra(1, 2, tabElement);
    graph.printElement(tabElement);
    
    delete[] tabElement;
    
    
    return 0;
}